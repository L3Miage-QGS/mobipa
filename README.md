# MobiPA
Application mobile (et ou web) de transport solidaire destinée au public des personnes âgées en s’appuyant sur l’API de l’application de Transport solidaire développée par l’entreprise Mobicoop, partenaire du projet.

# Pour commencer 💻

Pour utiliser le projet vous devez installer npm. 
Vous pouvez vérifier si vous l'avez déja en executant:

```npm -v```

# Installation des dépendances 📥
- Si c'est la première fois que vous lancez le projet:

```npm install -g npm```

- Sinon :

```npm install```

# Lancement du projet  🚀 
Pour lancer l'application éxecutez:

```npm run serve:mobipa```

Puis rendez vous sur: http://localhost:8080
